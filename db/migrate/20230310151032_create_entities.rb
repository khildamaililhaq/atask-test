class CreateEntities < ActiveRecord::Migration[7.0]
  def change
    create_table :entities do |t|
      t.string :name, presence: true
      t.string :password_digest
      t.string :type, index: true
      t.string :username, presence: true
      t.integer :parent_id, index: true

      t.timestamps
    end
  end
end
