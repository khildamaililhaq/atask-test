class CreateTeamDetails < ActiveRecord::Migration[7.0]
  def change
    create_table :team_details do |t|
      t.integer :team_id, null: false, index: true
      t.string :email
      t.string :code

      t.timestamps
    end
  end
end
