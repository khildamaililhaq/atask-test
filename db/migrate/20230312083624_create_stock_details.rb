class CreateStockDetails < ActiveRecord::Migration[7.0]
  def change
    create_table :stock_details do |t|
      t.integer :stock_id, null: false, index: true
      t.string :code
      t.integer :current_price

      t.timestamps
    end
  end
end
