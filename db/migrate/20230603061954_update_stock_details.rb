class UpdateStockDetails < ActiveRecord::Migration[7.0]
  def change
    add_column :stock_details, :symbol, :string
    add_column :stock_details, :identifier, :string
    add_column :stock_details, :open, :float
    add_column :stock_details, :day_high, :float
    add_column :stock_details, :day_low, :float
    add_column :stock_details, :last_price, :float
    add_column :stock_details, :previous_close, :float
    add_column :stock_details, :change, :float
    add_column :stock_details, :p_change, :float
    add_column :stock_details, :year_high, :float
    add_column :stock_details, :year_low, :float
    add_column :stock_details, :total_traded_volume, :float
    add_column :stock_details, :total_traded_value, :float
    add_column :stock_details, :last_updated_time, :datetime
    add_column :stock_details, :per_change_365d, :float
    add_column :stock_details, :per_change_30d, :float
    remove_column :stock_details, :code, :string
    remove_column :stock_details, :current_price, :integer
  end
end
