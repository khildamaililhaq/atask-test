class UpdateEntities < ActiveRecord::Migration[7.0]
  def change
    add_column :entities, :balance, :integer
    add_column :entities, :last_wallet_transaction_id, :integer, index: true
    add_column :entities, :created_by, :integer, index: true
    add_column :entities, :updated_by, :integer, index: true
  end
end
