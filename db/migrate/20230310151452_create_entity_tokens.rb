class CreateEntityTokens < ActiveRecord::Migration[7.0]
  def change
    create_table :entity_tokens do |t|
      t.string :token, presence: true
      t.datetime :expired_at, presence: true
      t.belongs_to :entity, index: true

      t.timestamps
    end
  end
end
