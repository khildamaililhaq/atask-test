class UpdateWalletTransactions < ActiveRecord::Migration[7.0]
  def change
    add_column :wallet_transactions, :number, :string, unique: true
  end
end
