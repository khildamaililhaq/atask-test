class CreateUserDetails < ActiveRecord::Migration[7.0]
  def change
    create_table :user_details do |t|
      t.integer :user_id, null: false, index: true
      t.string :phone_number
      t.string :email
      t.date :birthdate
      t.string :birthplace
      t.text :address

      t.timestamps
    end
  end
end
