class GetStockPriceService
  def initialize
    api_key = ENV['RAPID_API_KEY']
    api_host = ENV['RAPID_API_HOST']
    @api_url = ENV['RAPID_API_URL']
    @headers = { "X-RapidAPI-Key" => api_key, "X-RapidAPI-Host" => api_host }
  end

  def api_call(action)
    url = @api_url + action
    RestClient.get(url, @headers)
  end

  def price_getter(indice, identifier = nil)
    @headers = @headers.merge({ params: { "Indices" => indice, Identifier: identifier } })
    response = api_call("price")
    puts JSON.parse(response.body)
  end

  def price_all_getter
    response = api_call("any")
    JSON.parse(response.body)
  end

  def update_stocks
    data = price_all_getter
    data.map do |stock|
      current_stock = Stock.find_by(username: stock["symbol"])
      if current_stock.present?
        update_stock(current_stock, stock)
      else
        insert_stock(stock)
      end
    end
  end

  private

  def update_stock(current_stock, updated_stock)
    status = current_stock.stock_detail
    status.update(detail_hash(updated_stock))
    puts "Stock #{current_stock.name} updated: #{status.update(detail_hash(updated_stock))}"
  end

  def detail_hash(stock)
    {
      symbol: stock["symbol"],
      identifier: stock["identifier"],
      open: stock["open"],
      day_high: stock["dayHigh"],
      day_low: stock["dayLow"],
      last_price: stock["lastPrice"],
      previous_close: stock["previousClose"],
      change: stock["change"],
      p_change: stock["pChange"],
      year_high: stock["yearHigh"],
      year_low: stock["yearLow"],
      total_traded_volume: stock["totalTradedVolume"],
      total_traded_value: stock["totalTradedValue"],
      last_updated_time: stock["lastUpdateTime"],
      per_change_365d: stock["perChange365d"],
      per_change_30d: stock["perChange30d"]
    }
  end

  def insert_stock(stock)
    item = Stock.new({
                       name: stock["identifier"],
                       username: stock["symbol"],
                       password: stock["symbol"],
                       stock_detail_attributes: detail_hash(stock)
                     })
    item.save
    puts "Stock #{item.name} created: #{item.errors.messages}"
  end

  def remove(string)
    string.gsub!(/ /, "") if string.include? " "
  end
end
