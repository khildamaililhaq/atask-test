class User < Entity
    belongs_to :team, optional: true, foreign_key: :parent_id

    has_one :user_detail, dependent: :destroy

    accepts_nested_attributes_for :user_detail
end
