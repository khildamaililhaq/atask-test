class Team < Entity
  has_many :users, foreign_key: :parent_id, dependent: :destroy

  has_one :team_detail, dependent: :destroy

  accepts_nested_attributes_for :team_detail
end
