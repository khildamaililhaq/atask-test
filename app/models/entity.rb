class Entity < ApplicationRecord
  before_create :encrypt_password
  has_many :tokens
  has_many :debits, as: :debitable, class_name: 'WalletTransaction'
  has_many :credits, as: :creditable, class_name: 'WalletTransaction'

  attribute :password

  validates_presence_of :name, :username, :password, on: :create
  validates_uniqueness_of :username

  def encrypt_password
    self.password_digest = crypt.encrypt_and_sign(password)
  end

  def crypt
    ActiveSupport::MessageEncryptor.new(Digest::SHA256.digest(ENV['ENCRYPTION_KEY']))
  end

  def authenticate(password)
    decrypted_password = crypt.decrypt_and_verify(password_digest)

    decrypted_password == password
  end

  def generate_token
    token = SecureRandom.hex(10)
    EntityToken.create(entity: self, token:, expired_at: 1.day.from_now)
  end

  def update_balance!
    update(balance: debits.sum(:amount) - credits.sum(:amount))
  end

  handle_asynchronously :update_balance!

  def wallet_transactions
    WalletTransaction.where("debitable_id = ? OR creditable_id = ?", id, id)
  end

  def current_balance
    debits.sum(:amount) - credits.sum(:amount)
  rescue StandardError
    0
  end

  def transfer(params)
    raise StandardError, id.to_i if current_balance.to_i < params[:amount].to_i

    @destination_entity = Entity.find(params[:entity_id])

    transaction = WalletTransaction.new wallet_hash(params)
    transaction.creditable = self
    transaction.debitable = @destination_entity
    raise StandardError, 'Failed to transfer' unless transaction.save

    transaction
  end

  def wallet_hash(params)
    {
      amount: params[:amount],
      transaction_date: Date.today
    }
  end

  def deposit(params)
    transaction = WalletTransaction.new wallet_hash(params)
    transaction.debitable = self
    raise StandardError, 'Failed to Deposit' unless transaction.save

    transaction
  end
end
