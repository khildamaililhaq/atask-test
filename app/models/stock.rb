class Stock < Entity
  belongs_to :user, foreign_key: :parent_id, optional: true
  belongs_to :team, foreign_key: :parent_id, optional: true

  has_one :stock_detail, dependent: :destroy
  accepts_nested_attributes_for :stock_detail
end
