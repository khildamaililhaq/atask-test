Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'

  # authentication

  namespace :auths do
    post :login
    post :logout
    get :me
  end

  namespace :api do
    resource :users, only: :show do
      collection do
        post :deposit
        post :transfer
      end
    end
    resource :teams, only: :show do
      collection do
        post :deposit
        post :transfer
      end
    end
    resource :stocks, only: :show do
      collection do
        post :deposit
        post :transfer
      end
    end
  end

  resources :entities, only: :index
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
