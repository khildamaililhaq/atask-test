
# Internal wallet transactional system || Atask Test

This Application is created using

- Ruby 3.2.0
- Rails 7.0.4.2
- Postgresql




## Installation and Running

For the installation process, first you need to install Ruby and Postgresql first, you can follow the instruction [here](https://gorails.com/setup/ubuntu/22.04) to install and ruby and postgresql and follow the next steps bellow.

- Enter the app directory, and type ```cp .env.example .env``` and it will created environtment file that look like this, and fill the variable with yours


```bash
    DB_PORT=5432
    DB_USER=db_user
    DB_PASSWORD=db_password
    DB_NAME_DEV=db_name_dev
    DB_NAME_TEST=db_name_test
    DB_NAME_PROD=db_name_prod
    RAPID_API_KEY=99d3201c84mshb970256e7b62acdp1946d5jsn460b863e2ad8
    RAPID_API_HOST=latest-stock-price.p.rapidapi.com
    RAPID_API_URL=https://latest-stock-price.p.rapidapi.com/


    ENCRYPTION_KEY=encryption_key
```
- Type ```bundle install``` to install required gem
- Type ```rails db:create``` to create new database required, and then run ```rails db:migrate``` to run migration process and lastly you can create database seeder with ```rails db:seed```
- To run the apps you can type in the terminal ```rails s && rails jobs:work```
- To seeds the stocks data you can type in terminal ```rake stock_price:update_all```
- to see the stock data you can type in terminal ```rake stock_price:get_price[yourIndice,youtIdentifiers]```
- To see the API documentation you can go to the ```{yourAppURL}/api-docs```