require 'rake'
desc "Task to update stock prices"

namespace :stock_price do
  desc "get stock prices"
  task :get_price, [:indice, :identifiers] => [:environment] do |_t, args|
    GetStockPriceService.new.price_getter(args[:indice], args[:identifiers])
  end

  desc "Update all stock prices"
  task update_all: :environment do
    GetStockPriceService.new.update_stocks
  end
end
